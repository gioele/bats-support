#!/usr/bin/env bats

bats_load_library "bats-support"

@test "batslib_ functions are available (positive)" {
	run batslib_is_single_line 'a' $'b\n'
	[ "$status" -eq 0 ]
}

@test "batslib_ functions are available (negative)" {
	run batslib_is_single_line 'a' $'b\nc'
	[ "$status" -eq 1 ]
}
